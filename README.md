# CMSIS core

We are not the author of this code, this repo exist only to provide git access to a small portion of code (without all the cmsis components), namely: cpu and compiler specific code supplied by [ARM](https://developer.arm.com/tools-and-software/embedded/cmsis)  
See [original repo](https://github.com/ARM-software/CMSIS_5) for more info
Contact [maintainer][maintainer_email] for information and/or questions

## Deployment

1. Add to **include** path of compiler/assembler/etc the _inc_ folder

[maintainer_email]:     <MAILTO:laurencedv@realee.tech>

